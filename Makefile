# If this is being run for production, then there should be an environment
# variable called APP_ENV set to "production". For example:
# APP_ENV=production make build
JEKYLL_OPTS=
ifeq ($(APP_ENV), production)
  JEKYLL_OPTS+=--config _config.yml,_config_prod.yml
endif

.PHONY: install build serve test clean
all: install build test

install:
	bundle install

build:
	bundle exec jekyll build $(JEKYLL_OPTS)

serve:
	bundle exec jekyll serve $(JEKYLL_OPTS)

# If the site uses a subfolder (aka "baseurl" in the Jekyll config) then it
# should be set as an environment variable called SUBFOLDER. For example:
# SUBFOLDER=my-subolder make test
test:
	# Note that we go to all this trouble because htmlproofer doesn't work well
	# with Jekyll's subfolders.
	mkdir -p ./_test/$(SUBFOLDER)
	cp -r ./_site/* ./_test/$(SUBFOLDER)/
	touch ./_test/index.html
	bundle exec htmlproofer --disable-external ./_test
	rm -rf ./_test

clean:
	rm -fr _site
